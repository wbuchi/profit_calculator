# TODO

- bitzenyのprofitabilityの算出
- Original Yescrypt-0.5
- 時系列データの扱い方がよくわからなかったため，https://facebook.github.io/prophet/ を使用．parameterがよくわからないので要調査
- ~~jsonを取得してくる間隔を決める．(15min, 30min, 1h, 4h, 24h)~~
- ~~webAPIの作成 (flaskで作れそう)~~

20180713

- bitzenyのprofitabilityの算出ができないので，取引所からの価格取得． -> 24h で採掘できる bitzenyの量を算出する． -> 24h で採掘できるbitcoinの量を算出，他のコインと比較する？

# setup

    pip install -r requirements.txt

or

    pip3 install -r requirements.txt

# web app 

    python3 web/app.py

example

    curl -i http://localhost:5000/api/v1/coins
    HTTP/1.0 200 OK
    Content-Type: application/json
    Content-Length: 165
    Server: Werkzeug/0.14.1 Python/3.7.0
    Date: Fri, 13 Jul 2018 08:52:00 GMT

    [
      {
        "24h_profit_btc": "0.00034243",
        "coins": "mona",
        "id": 0
      },
      {
        "24h_profit_btc": "0.00016229",
        "coins": "zcash",
        "id": 1
      }
    ]

    curl -i http://localhost:5000/api/v1/best_coin
    HTTP/1.0 200 OK
    Content-Type: application/json
    Content-Length: 7
    Server: Werkzeug/0.14.1 Python/3.7.0
    Date: Fri, 13 Jul 2018 08:50:58 GMT

    "mona"

# ipynb



```python
import pandas as pd
import json
from pprint import pprint
import pandas as pd
import csv

import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
```


```python
headers = ["datetime", "etc_revenue", "mona_revenue", "zcash_revenue"]

df = pd.read_csv('output.csv', names = headers)

df['datetime'] = pd.to_datetime(df['datetime'])
```


```python
df['datetime']
```




    0   2018-06-29 16:39:21.929307
    1   2018-06-29 16:39:23.002335
    2   2018-06-29 16:39:24.068362
    3   2018-06-29 16:39:25.135565
    4   2018-06-29 16:39:26.203554
    Name: datetime, dtype: datetime64[ns]




```python
x = ['datetime']
y = ['etc_revenue']
```


```python
df = df.sort_values('datetime', ascending=True)
plt.plot(df['datetime'], df['etc_revenue'])
plt.plot(df['datetime'], df['mona_revenue'])
plt.plot(df['datetime'], df['zcash_revenue'])
plt.xticks(rotation='vertical')
```




    (array([736874.69400134, 736874.69401291, 736874.69402449, 736874.69403606,
            736874.69404763]), <a list of 5 Text xticklabel objects>)




![png](output_4_1.png)



```python
days      = mdates.DayLocator()  # every day
daysFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_locator(days)
ax.xaxis.set_major_formatter(daysFmt)
fig.autofmt_xdate()
```


```python
plt.savefig('./out.png')
```


    <Figure size 432x288 with 0 Axes>



```python
import pandas as pd
import json
from pprint import pprint
import pandas as pd
import scipy as sp
import csv

import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
from sklearn import preprocessing
from fbprophet import Prophet
```


```python
headers = ["ds", "y"]

df = pd.read_csv('output.csv', names = headers)

df['ds'] = pd.to_datetime(df['ds'])
```


```python
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ds</th>
      <th>y</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2018-07-04 16:43:09.026372</td>
      <td>0.000043</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2018-07-05 16:43:10.094603</td>
      <td>0.000043</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2018-07-06 16:43:11.155122</td>
      <td>0.000043</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2018-07-07 16:43:12.204823</td>
      <td>0.000043</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2018-07-08 16:43:13.263053</td>
      <td>0.000043</td>
    </tr>
  </tbody>
</table>
</div>




```python
df['y'] = np.log(df['y'])

m = Prophet()

m.fit(df)
```

    INFO:fbprophet.forecaster:Disabling yearly seasonality. Run prophet with yearly_seasonality=True to override this.
    INFO:fbprophet.forecaster:Disabling weekly seasonality. Run prophet with weekly_seasonality=True to override this.
    INFO:fbprophet.forecaster:Disabling daily seasonality. Run prophet with daily_seasonality=True to override this.
    INFO:fbprophet.forecaster:n_changepoints greater than number of observations.Using 7.0.
    /usr/local/lib/python3.6/site-packages/fbprophet/forecaster.py:353: DeprecationWarning: object of type <class 'numpy.float64'> cannot be safely interpreted as an integer.
      np.linspace(0, hist_size - 1, self.n_changepoints + 1)





    <fbprophet.forecaster.Prophet at 0x11933a0f0>




```python
future = m.make_future_dataframe(periods=10)
future.tail()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ds</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>15</th>
      <td>2018-07-19 16:43:18.555196</td>
    </tr>
    <tr>
      <th>16</th>
      <td>2018-07-20 16:43:18.555196</td>
    </tr>
    <tr>
      <th>17</th>
      <td>2018-07-21 16:43:18.555196</td>
    </tr>
    <tr>
      <th>18</th>
      <td>2018-07-22 16:43:18.555196</td>
    </tr>
    <tr>
      <th>19</th>
      <td>2018-07-23 16:43:18.555196</td>
    </tr>
  </tbody>
</table>
</div>




```python
forecast = m.predict(future)
forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ds</th>
      <th>yhat</th>
      <th>yhat_lower</th>
      <th>yhat_upper</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>15</th>
      <td>2018-07-19 16:43:18.555196</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
    </tr>
    <tr>
      <th>16</th>
      <td>2018-07-20 16:43:18.555196</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
      <td>-10.051523</td>
    </tr>
    <tr>
      <th>17</th>
      <td>2018-07-21 16:43:18.555196</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
      <td>-10.051523</td>
    </tr>
    <tr>
      <th>18</th>
      <td>2018-07-22 16:43:18.555196</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
      <td>-10.051523</td>
    </tr>
    <tr>
      <th>19</th>
      <td>2018-07-23 16:43:18.555196</td>
      <td>-10.051524</td>
      <td>-10.051524</td>
      <td>-10.051523</td>
    </tr>
  </tbody>
</table>
</div>




```python
m.plot(forecast)
```




![png](output_6_0.png)




![png](output_6_1.png)



```python
m.plot_components(forecast);
```


![png](output_7_0.png)



```python
x = ['datetime']
y = ['mona_revenue']
```


```python
df = df.sort_values('datetime', ascending=True)
```


```python
plt.plot(df['count'], df['mona_revenue'])
plt.plot(df['count'], df['zcash_revenue'])
plt.xticks(rotation='vertical')
```


```python
x = np.array(df['count'])
y_mona = np.array(df['mona_revenue'])
y_zcash = np.array(df['zcash_revenue'])
```


```python
plt.plot(x, np.poly1d(np.polyfit(x, y_mona, 3))(x))
plt.plot(df['count'], df['mona_revenue'])
plt.plot(x, np.poly1d(np.polyfit(x, y_zcash, 3))(x))
plt.plot(df['count'], df['zcash_revenue'])
```
