import flask
from flask import request, jsonify
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True



# Create some test data for our catalog in the form of a list of dictionaries.
books = [
    {'id': 0,
     'title': 'A Fire Upon the Deep',
     'author': 'Vernor Vinge',
     'first_sentence': 'The coldsleep itself was dreamless.',
     'year_published': '1992'},
    {'id': 1,
     'title': 'The Ones Who Walk Away From Omelas',
     'author': 'Ursula K. Le Guin',
     'first_sentence': 'With a clamor of bells that set the swallows soaring, the Festival of Summer came to the city Omelas, bright-towered by the sea.',
     'published': '1973'},
    {'id': 2,
     'title': 'Dhalgren',
     'author': 'Samuel R. Delany',
     'first_sentence': 'to wound the autumnal city.',
     'published': '1975'}
]

# class WhatToMineAPI():
#     def __init__(self):
#         url = "https://whattomine.com/coins.json"
#         r = requests.get(url)
#         d = r.json()
#         return d

# @app.route('/api/coin', methods=['GET'])
# def api_coin():
#     return WhatToMineAPI()

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''


# A route to return all of the available entries in our catalog.
@app.route('/api/v1/coins', methods=['GET'])
def api_all():
    url = "https://whattomine.com/coins.json"
    r = requests.get(url)
    d = r.json()
    mona_revenue = d['coins']['Monacoin']['btc_revenue']
    zcash_reveenue = d['coins']['Zcash']['btc_revenue']
    # coin_list.sort()
    
    coin_list = [
        {'id': 0,
         'coins': 'mona',
         '24h_profit_btc': mona_revenue, 
        },
        {'id':1,
         'coins': 'zcash',
         '24h_profit_btc': zcash_reveenue, 
        }
    ]
    sorted(coin_list, key=lambda x:x['24h_profit_btc'])

    return jsonify(coin_list)

@app.route('/api/v1/best_coin', methods=['GET'])
def api_best_coin():
    url = "https://whattomine.com/coins.json"
    r = requests.get(url)
    d = r.json()
    
    mona_revenue = d['coins']['Monacoin']['btc_revenue']
    zcash_reveenue = d['coins']['Zcash']['btc_revenue']

    if mona_revenue < zcash_reveenue:
        best_coin = "zcash"
    else:
        best_coin = "mona"
    
    coin_list = [
        {'id': 0,
         'coins': 'mona',
         '24h_profit_btc': mona_revenue, 
        },
        {'id':1,
         'coins': 'zcash',
         '24h_profit_btc': zcash_reveenue, 
        }
    ]
    sorted(coin_list, key=lambda x:x['24h_profit_btc'])

    return jsonify(best_coin)

app.run()
