import flask
from flask import request, jsonify
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def home():
    return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''

# A route to return all of the available entries in our catalog.
@app.route('/api/v1/coins', methods=['GET'])
def api_all():
    url = "https://whattomine.com/coins.json"
    r = requests.get(url)
    d = r.json()
    mona_revenue = d['coins']['Monacoin']['btc_revenue']
    zcash_reveenue = d['coins']['Zcash']['btc_revenue']
    # coin_list.sort()
    
    coin_list = [
        {'id': 0,
         'coins': 'mona',
         '24h_profit_btc': mona_revenue, 
        },
        {'id':1,
         'coins': 'zcash',
         '24h_profit_btc': zcash_reveenue, 
        }
    ]
    sorted(coin_list, key=lambda x:x['24h_profit_btc'])

    return jsonify(coin_list)

@app.route('/api/v1/best_coin', methods=['GET'])
def api_best_coin():
    url = "https://whattomine.com/coins.json"
    r = requests.get(url)
    d = r.json()
    
    mona_revenue = d['coins']['Monacoin']['btc_revenue']
    zcash_reveenue = d['coins']['Zcash']['btc_revenue']

    if mona_revenue < zcash_reveenue:
        best_coin = "zcash"
    else:
        best_coin = "mona"
    
    coin_list = [
        {'id': 0,
         'coins': 'mona',
         '24h_profit_btc': mona_revenue, 
        },
        {'id':1,
         'coins': 'zcash',
         '24h_profit_btc': zcash_reveenue, 
        }
    ]
    sorted(coin_list, key=lambda x:x['24h_profit_btc'])

    return jsonify(best_coin)

app.run()
