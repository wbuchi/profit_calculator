import requests
import pandas as pd
import plotting
import time
import datetime
import json
import csv

url = 'https://whattomine.com/coins.json'
file = open('data.txt', 'w')

while True:
    print("This prints once a minute.")
    # time.sleep(1800) # Delay for 30 minute (30 * 60 seconds).
    time.sleep(2)

    r = requests.get(url)
    d = r.json()

    now_time = datetime.datetime.now()

    mona_revenue = d['coins']['Monacoin']['btc_revenue']
    zcash_reveenue = d['coins']['Zcash']['btc_revenue']

    coin_list = [now_time, mona_revenue, zcash_reveenue]

    print(coin_list)

    with open("output.csv", "a") as f:
        writer = csv.writer(f, delimiter=',', quoting=csv.QUOTE_ALL)
        writer.writerow(coin_list)
